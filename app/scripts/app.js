'use strict';

/**
 * @ngdoc overview
 * @name javascriptApp
 * @description
 * # javascriptApp
 *
 * Main module of the application.
 */
angular
  .module('javascriptApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/three',{
        templateUrl:'views/threeJs.html',
        controller:'ThreeCtrl',
        controllerAs:'three'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
