'use strict';


/**
 * @ngdoc function
 * @name javascriptApp.controller:ThreeCtrl
 * @description
 * # ThreeCtrl
 * Controller of the javascriptApp
 */
angular.module('javascriptApp')
  .controller('ThreeCtrl', ['$scope', function ($scope) {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $scope.canvasWidth = 400;
    $scope.canvasHeight = 400;
}]);


angular.module('javascriptApp')
  .directive('ngThree',function(){
      return {
        restrict:'EA',
        scope:{
          'width'  : '=',
          'height' : '=',
        },
        link: function postLink(scope,elem,attr){
          
          
          var camera, scene, renderer,
          contW = (scope.fillcontainer) ?  elem[0].clientWidth : scope.width,
          contH = scope.height ;


          scope.init = function () {
            
             console.log(attr);
             //Camera
             camera = new THREE.PerspectiveCamera( 75,contW/contH, 0.1, 1000 );
             camera.position.z = 5;
             
             //Scene
             scene = new THREE.Scene();
             
             //Renderer
             renderer = new THREE.WebGLRenderer();
             renderer.setSize( contW,contH);
             
             
             var geometry = new THREE.BoxGeometry( 1, 1, 1 );
             var material = new THREE.MeshBasicMaterial( { color: 0xC2F148 } );
             var cube = new THREE.Mesh( geometry, material );
             scene.add( cube );

             
             // element is provided by the angular directive
             elem[0].appendChild( renderer.domElement );

          };
          
          // -----------------------------------
          // Draw and Animate
          // -----------------------------------
          scope.animate = function () {

            requestAnimationFrame( scope.animate );

            scope.render();

          };

          scope.render = function () {

            //camera.position.x += ( mouseX - camera.position.x ) * 0.05;
            // camera.position.y += ( - mouseY - camera.position.y ) * 0.05;
            //camera.lookAt( scene.position );
            
            renderer.render( scene, camera );

          };
          
          
          //----------- Execution----- 
          scope.init(); 
          scope.animate();
        }
      
      };
  });
