'use strict';

/**
 * @ngdoc function
 * @name javascriptApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the javascriptApp
 */
angular.module('javascriptApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
